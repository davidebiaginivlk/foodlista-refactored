import * as admin from 'firebase-admin'
import { Role, ProtectedEntity, Permission } from './types/Permission'
import * as cors from 'cors'

const db = admin.firestore(),
      corsMiddleware = cors({ origin: true })


export const operatorEnablerFactory = 
    (entity: ProtectedEntity) =>
        async ({ operatorId, entityId, role }: Permission) => {
            const operatorRef = db.collection('operators').doc(operatorId),
                  target = entity + 's'

            await db.runTransaction(async (transaction): Promise<any> => {
                await transaction.update(operatorRef, {
                [target]: {
                        [entityId]: role 
                    }
                })
            })

            const { customClaims } = await admin.auth().getUser(operatorId)

            const update = {
                    ... customClaims,
                [target]: { 
                    ... (customClaims && customClaims[target]),
                    [entityId]: role 
                }
            }

            await admin.auth().setCustomUserClaims(operatorId, update)
        }

export const letsTry = (request: any, response: any) => {
    return corsMiddleware(request, response, async () => {
        const operatorId = 'e6aRVtgRaQQAGn6UbnAHFO7cJmk1'
    
        await admin.auth().setCustomUserClaims(operatorId, {})

        await operatorEnablerFactory(ProtectedEntity.RESTAURANT)({ 
            operatorId, 
            entityId: 'restaurant-0',
            role: Role.ADMIN
        })

        response.send(200)
    })
}