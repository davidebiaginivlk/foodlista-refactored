import * as admin from 'firebase-admin'

const serviceAccount = require("../SA.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://foodlista-refactored.firebaseio.com"
})

import * as functions from 'firebase-functions';
import { writeMockData } from './writeMockData'
import { letsTry } from './enableOperator'

exports.writeMockData = functions.https.onRequest(writeMockData)
exports.letsTry = functions.https.onRequest(letsTry)