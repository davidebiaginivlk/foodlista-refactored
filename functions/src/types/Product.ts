export enum ProductState {
    ACTIVE = 'ACTIVE',
    DRAFT = 'DRAFT',
}

export type Price = {
  id: string;
  note?: string;
  name?: string;
  unit?: string;
  price: string;
  label?: string;
  currency?: string;
}

export type Product = {
  id: string;
  slug: string; /* @added */
  name: string;
  state: ProductState;
  flow?: string;
  image?: string;
  prices: Price[];
  organizationId: string;
  categoryId: string;
  allergens: string[];
  description: string;
  certifications: string[];
  note: string;
}