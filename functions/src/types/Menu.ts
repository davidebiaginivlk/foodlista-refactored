export enum ItemTemplate {
    COMPLETE = 'COMPLETE',
    NO_IMAGE = 'NO IMAGE',
    SIMPLE = 'SIMPLE',
    WITH_VARIANTS = 'WITH VARIANTS',
    LABEL = 'LABEL',
    NOTE = 'NOTE'
  };
  
  export type MenuSection = {
    name: string;
    label: string;
    template: ItemTemplate;
    itemIds: string[];
  };
  
  export type MenuPage = {
    /* @removed id: string; */
    label: string;
    sections: MenuSection[];
    note: string;
  };
  
  export type Menu = {
    /* @replaced name: string; with slug */
    slug: string;
    label: string;
    restaurantIds: string[]; /* @renamed */
    pages: MenuPage[];
    note: string;
  };