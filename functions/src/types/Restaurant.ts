export type RestaurantServices = {
    [key: string]: {
      name: string;
      description?: string;
      price?: number;
    };
  };
  
  export type SupportedSocial = 'facebook' | 'instagram';
  
  export type Social = {
    [K in SupportedSocial]?: string;
  };
  
  export type Address = {
    city: string;
    street: string;
    country: string;
    zipCode: string;
  };
  
  export type Restaurant = {
    id: string;
    slug: string; /* @added */
    organizationId: string; /* @added */
    name: string;
    logo?: string;
    type: string;
    color: string;
    email: string;
    phone?: string;
    socials?: Social;
    services?: RestaurantServices;
    address: Address;
    description?: string;
    coverCharge?: number;
  };