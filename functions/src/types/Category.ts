export type Category = {
    id: string;
    name: string;
    image?: string;
    /**
     * Per risolvere il problema di gestire un eventuale sistema multi-sede,
     * la soluzione potrebbe essere scaricare nello stato dell'applicazione back-office 
     * tutte le categorie di tutti i ristoranti per cui l'operatore corrente è abilitato.
     **/
    restaurantId: string;
}