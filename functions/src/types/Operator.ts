import { Role, EntityACL } from "./Permission"


export type Operator = {
    name: string,
    id: string,
    organizations: EntityACL,
    restaurants: EntityACL
}