export enum Role {
    ADMIN = 'admin'
}

export enum ProtectedEntity {
    ORGANIZATION = 'organization',
    RESTAURANT = 'restaurant'
}

export type Permission = {
    operatorId: string,
    entityId: string, 
    role: Role
}

export type EntityACL = {
    [id: string]: Role
}
