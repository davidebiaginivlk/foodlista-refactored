import * as admin from 'firebase-admin'

import {
    Operator,
    Restaurant,
    Category,
    Product,
    ProductState,
    Menu,
    ItemTemplate
} from './types'
import { Role } from './types/Permission'

const db = admin.firestore()

const operator: Operator = {
  id: 'e6aRVtgRaQQAGn6UbnAHFO7cJmk1',
  name: 'Davide Biagini',
  permissions: {
    'restaurant-0': Role.ADMIN
  }
}

const restaurant: Restaurant = {
  id: 'restaurant-0',
  slug: 'historical-pizza-e-ghiotterie',
  name: 'Historical Pizza & Ghiotterie',
  type: 'pizza-shop',
  organizationId: 'DxZ671KbgLXFZm4m9c4g',
  color: '#aa0000',
  email: 'mock@foodlista.app',
  address: {
    street: 'Via Napoli, 15',
    city: 'Pistoia (PT)',
    country: 'Toscana',
    zipCode: '51100'
  }
}

const product: Product = {
  id: 'product-0',
  slug: 'pizza-masaniello',
  name: 'Pizza Masaniello',
  categoryId: 'pizze-storiche',
  state: ProductState.ACTIVE,
  prices: [{
    id: 'price-0',
    price: '7,80',
    currency: '€'
  }],
  organizationId: 'DxZ671KbgLXFZm4m9c4g',
  allergens: [],
  description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur...",
  certifications: [],
  note: "Molto piccante."
}

const category: Category = {
  id: 'pizze-storiche',
  name: 'Storiche',
  restaurantId: 'restaurant-0'
}

const menu: Menu = {
  slug: 'il-nostro-menu',
  label: 'Il nostro menu',
  restaurantIds: ['restaurant-0'],
  pages: [
    {
      label: 'Pizze',
      note: '',
      sections: [
        {
          name: 'classic-pizzas',
          label: 'Pizze Classiche Napoletane',
          template: ItemTemplate.SIMPLE,
          itemIds: ['product-0']
        }
      ]
    }
  ],
  note: ''
}

export const writeMockData = async (request: any, response: any) => {
  const batch = db.batch()
  
  operator;
  /*
  batch.set(
    db.collection('operators').doc('e6aRVtgRaQQAGn6UbnAHFO7cJmk1'),
    operator
  ) */

  batch.set(
    db.collection('restaurants').doc('restaurant-0'),
    restaurant
  )

  batch.set(
    db.collection('products').doc('product-0'),
    product
  )

  batch.set(
    db.collection('categories').doc(),
    category
  )

  batch.set(
    db.collection('menus').doc(),
    menu
  )

  await batch.commit()

  response.send(200);
}